#include <iostream>
#include <math.h>

// int main() {
//     int target, desc_index = 0, asc_index = 1;
//     int sum, times, sequence_result, result = 0;

//     std::cout << "Enter n: ";
//     std::cin >> target;

//         if (target > 2) desc_index = ceil((float) target/2);

//         while (desc_index > 0) {

//             while (asc_index < desc_index) {
//                 sum = desc_index + asc_index;
//                 times = desc_index - asc_index + 1;
//                 sequence_result = sum * times / 2;

//                 // std::cout << desc_index << " - " << asc_index << " = " << sequence_result << " * " << controll <<  '\n';

//                 if (sequence_result == target) {
//                     result++;
//                     std::cout << sequence_result << " = " << asc_index << " - " << desc_index << '\n';
//                 }
//                 asc_index++;
                
//             }
            
//             asc_index = 1;
//             desc_index--;
//         }
        
//         std::cout << result << '\n';

//     return 0;
// }

int main() {
    long long int target, desc_index = 0, asc_index = 1, major_result = 0, target_of_major = 0;
    long long int sum, times, sequence_result, result = 0, controll = 0;
    float speedController = 1;

    std::cout << "Enter n: ";
    std::cin >> target;

    while (target > 2) {
        result = 0;

        if (target > 2) {
            desc_index = ceil((float) target/2);
            asc_index = ceil((float) desc_index/(1 + (float) desc_index/target));
        }

        while (desc_index > 0) {
            speedController = (float) desc_index/target;

            while (asc_index > 0) {
                sum = desc_index + asc_index;
                times = desc_index - asc_index + 1;
                sequence_result = sum * times / 2;
                if (sequence_result == target) 
                {                
                    result++;
                    break;
                } else if (sequence_result > target) {
                    if (controll) break;

                    asc_index = ceil((float) asc_index * (1 + (1 - speedController)));
                    if (asc_index > desc_index) asc_index = desc_index;
                } else {
                    asc_index--;
                    controll = 1;
                }
            }

            controll = 0;
            desc_index--;
            asc_index = ceil((float) desc_index/(1 + speedController));
        }

        if (major_result < result) {
            major_result = result;
            target_of_major = target;
        }
        
        target--;
        std::cout << target << '\n';

    }
        
    std::cout << target_of_major << " : " << major_result << '\n';

    return 0;
}
